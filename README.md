- Hello! This is my machine learning project that is designed to be able to predict the loan eligibility of a user. I used a dataset that I obtained from kaggle.com and can be obtained here: https://www.kaggle.com/datasets/ninzaami/loan-predication?resource=download


- I trained the dataset using google colab research and I used a machine learning model made available through an sklearn library. Using linear regression I was able to train my machine learning model with my dataset and ultimately acheived a prediction accuracy of 83%. The google colab file can be accessed here: https://colab.research.google.com/drive/1QYh3xM-4ACcNg_yC_w5Hhag1vPa4aW9q?usp=sharing

- The technologies used for this application are: 
    1. Django
    2. Sklearn
    3. Python 3
    4. Numpy
    5. pandas
    6. seaborn

- Please checkout the deployed application here: https://mlfinancial-loan-app.herokuapp.com/loans/