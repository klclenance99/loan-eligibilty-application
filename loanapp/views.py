from django.shortcuts import render
import numpy as np
import pickle

with open('loanapp/model_pkl', 'rb') as f:
    model = pickle.load(f)
# Create your views here.
def homeview(request):
    return render(request, "loan/home.html")

def prediction_form_view(request):
    if request.method == "GET":
        return render(request, "loan/form.html")
    else:
        d = np.zeros(11)
        d[0] = int(request.POST['genders'])
        d[1] = int(request.POST['married'])
        d[2] = int(request.POST['dependents_list'])
        d[3] = int(request.POST['education'])
        d[4] = int(request.POST['employment'])
        d[5] = int(request.POST['applicantIncome']) // 10
        d[6] = int(request.POST['coapplicantIncome']) // 10
        d[7] = int(request.POST['loan_amnt']) // 10
        d[8] = int(request.POST['term_d'])
        d[9] = int(request.POST['crhist_list'])
        d[10] = int(request.POST['property_list'])
        d = d.reshape(1, -1)
        prediction = model.predict(d)[0]
        if prediction == 1:
            res = 'Congratulations! Your loan has a high chance of being approved🎉🎉🎉'
        else:
            res = "Sorry, Your loan can't be approved😞😞😞"
        return render(request,"loan/form.html", {"result": res})