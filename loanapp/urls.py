from django.urls import path
from .views import homeview, prediction_form_view

urlpatterns = [
    path("",homeview ,name="home"),
    path("form/", prediction_form_view, name="form"),
]